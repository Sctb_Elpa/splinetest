#include <QDateTime>

#include "stocktimescaledraw.h"

StockTimeScaleDraw::StockTimeScaleDraw(const QString& fmt) :
    format(fmt)
{
}

QwtText StockTimeScaleDraw::label(double v) const
{
    QDateTime t = QDateTime().addMSecs((unsigned long)v);
    return t.toString(format);
}


