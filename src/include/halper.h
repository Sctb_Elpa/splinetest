#include <QObject>
#include <qwt_plot_zoomer.h>
#include <qwt_plot.h>
#include <qwt_legend.h>
#include <qwt_legend_label.h>
#include <qwt_legend_data.h>

class halper : public QObject
{
    Q_OBJECT


private:
    QwtPlot *plot;
    QwtPlotZoomer *zoom;
    QwtLegend* legend;

public:
    halper(QwtPlot *plot, QwtLegend* legend, QObject *parent = NULL) :
        QObject(parent)
    {
        this->plot = plot;
        this->legend = legend;
        connect(legend, SIGNAL(checked(QVariant,bool,int)), this,
                SLOT(showCurve(QVariant,bool,int)));
    }

    ~halper()
    {
        delete zoom;
    }

public slots:
    void timerSlot()
    {
        zoom = new QwtPlotZoomer(plot->canvas());
    }

    void showCurve(QVariant iteminfo, bool on, int index)
    {
        Q_UNUSED(index);
        QwtPlotItem *item = plot->infoToItem(iteminfo);
        item->setVisible(on);
        plot->replot();
        ((QwtLegendLabel*)legend->legendWidget(iteminfo))->setChecked(on);
    }
};
