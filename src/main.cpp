#include <math.h>

#include <QApplication>
#include <QDateTime>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QStringList>
#include <QTimer>
#include <QVector>

#include <QDebug>

#include <qwt_legend.h>
#include <qwt_legend_data.h>
#include <qwt_legend_label.h>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_renderer.h>
#include <qwt_plot_textlabel.h>
#include <qwt_scale_widget.h>
#include <qwt_symbol.h>

#include <SmoothSpline.h>

#include "ap.h"
#include "interpolation.h"
#include "stocktimescaledraw.h"

#include "halper.h"

#define CUTOFF_VALUES_ABOVE (2.0)

enum enDataFormat { OLD, R4_7, R4_9 };

#define __DIVIDE_INTERVAL (500)
#define __STEP(min, max) ((max - min) / __DIVIDE_INTERVAL)
#define __SIGMA_R(step) (step / 4)

class PlotTextLabel : public QwtPlotTextLabel {
protected:
  void draw(QPainter *painter, const QwtScaleMap &xMap, const QwtScaleMap &yMap,
            const QRectF &canvasRect) const {
    if (painter->paintEngine()->type() == QPaintEngine::Picture) {
      const int m = margin();

      const QRectF rect = textRect(canvasRect.adjusted(m, m, -m, -m),
                                   text().textSize(painter->font()));

      text().draw(painter, rect);
    } else {
      QwtPlotTextLabel::draw(painter, xMap, yMap, canvasRect);
    }
  }
};

static double AW(double t) {
  t = t /*/ 1000.0*/ + 273.16; // в кельвинах
  // return exp((-27.622 + 528.373 * (1 / (273.16 + t)) + 4.579 * log(237.16 +
  // t))) - 0.000753;
  double r1 = 4.579 * log(t) - 27.622;
  double r2 = r1 + 528.373 / t;
  return exp(r2);
}

static QVector<double> findD2YZeros(QVector<QPointF> d2Y) {
  int l = d2Y.size();
  QVector<double> result;
  for (int i = 1; i < l; ++i) {
    if (d2Y.at(i - 1).y() * d2Y.at(i).y() <= 0) {
      result.append((d2Y.at(i).x() + d2Y.at(i - 1).x()) / 2);
    }
  }
  return result;
}

/*
static QwtPlot* buildPlot(const QVector<double>& x, const QVector<double>& y,
alglib::spline1dinterpolant &alSpline)
{
    QwtPlot *plot = new QwtPlot;
    plot->enableAxis(QwtPlot::yRight);
    plot->setCanvasBackground(QColor(Qt::white));
    plot->setStyleSheet("background-color:white;");

    QVector<QPointF> d[3];

    double* x_ = new double[x.size()];
    double *y_ = new double[y.size()];

    memcpy(x_, x.data(), x.size()* sizeof(double));
    memcpy(y_, y.data(), y.size()* sizeof(double));

    int l = x.size();

    spline_range *mSplineCoeffs; //= new spline_range[l - 1];

    SplineApprox(x_, y_, l, 50, &mSplineCoeffs);

    double step = __STEP(x.at(0), x.last());
    double xi = (double)x_[0];
    int t = 0;
    while (xi < x_[l - 1])
    {
        calc_result res = calculateSplineAt(mSplineCoeffs, l - 1, xi);

        double val, d1v, d2v;
        alglib::spline1ddiff(alSpline, xi, val, d1v, d2v);
        d[0].append(QPointF(xi, res.Y));
        d[1].append(QPointF(xi, res.d1Y));
        d[2].append(QPointF(xi, res.d2Y * 50));
        xi += step;
    }
    QVector<double> d2yZeros = findD2YZeros(d[2]);
    QVector<QPointF> d1YLocalMax, YBands;

    double Global_d1y_Max = -INFINITY;

    foreach(double d, d2yZeros)
    {
        if ((d <= x.first()) || (d >= x.last()))
            continue;
        calc_result res_P = calculateSplineAt(mSplineCoeffs, l - 1, d);
        calc_result res_bP = calculateSplineAt(mSplineCoeffs, l - 1, d - step);
        calc_result res_aP = calculateSplineAt(mSplineCoeffs, l - 1, d + step);

        if ((res_P.d1Y > res_bP.d1Y) && (res_P.d1Y > res_aP.d1Y))
        {
            d1YLocalMax.append(QPointF(d, res_P.d1Y));
            if (res_P.d1Y > Global_d1y_Max)
                Global_d1y_Max = res_P.d1Y;
            YBands.append(QPointF(d, res_P.Y));
        }
    }

    // Удалить глобальный максимум из списка локальных
    QPointF d1YGlobalMax[2];
    t = 0;
    foreach(QPointF p, d1YLocalMax)
    {
        if (p.y() == Global_d1y_Max)
        {
            d1YGlobalMax[0] = p;
            d1YGlobalMax[1] = YBands.at(t);
            d1YLocalMax.remove(t);
            YBands.remove(t);
            break;
        }
        ++t;
    }

    delete x_;
    delete y_;
    delete mSplineCoeffs;

    QwtPlotCurve* curve;
    QPen pen;
    QwtSymbol* symbol;

    for (int i = 0; i < 2; ++i)
    {
        pen = QPen(QColor::fromHsvF((float)i / 3, 1.0, 1.0));;
        symbol = new QwtSymbol;
        curve = new QwtPlotCurve(QString::number(i));
        symbol->setStyle(QwtSymbol::Cross);
        symbol->setPen(pen);
        symbol->setSize(5);
        curve->setRenderHint(QwtPlotItem::RenderAntialiased);
        curve->setPen(pen);
        curve->setSymbol(symbol);
        curve->attach(plot);

        if (i == 0)
            curve->setAxes(QwtPlot::xBottom, QwtPlot::yLeft);
        else
            curve->setAxes(QwtPlot::xBottom, QwtPlot::yRight);

        curve->setSamples(d[i]);
    }
#if 0
    //Zero D2Y line
    pen = QPen(Qt::black);;
    symbol = new QwtSymbol;
    curve = new QwtPlotCurve("Zero");
    symbol->setStyle(QwtSymbol::NoSymbol);
    symbol->setPen(pen);
    symbol->setSize(5);
    curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve->setPen(pen);
    curve->setSymbol(symbol);
    curve->attach(plot);
    curve->setAxes(QwtPlot::xBottom, QwtPlot::yRight);
    double tx[] = {d[0].at(0).x(), d[0].at(d[0].size() - 1).x()};
    double ty[] = {0, 0};
    curve->setSamples(tx, ty, 2);
#endif
    // D1Y local maximums
    pen = QPen(Qt::red);;
    symbol = new QwtSymbol;
    curve = new QwtPlotCurve(QObject::trUtf8("Локальные максимумы"));
    symbol->setStyle(QwtSymbol::Ellipse);
    symbol->setPen(pen);
    symbol->setBrush(QBrush(Qt::NoBrush));
    symbol->setSize(10);
    curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve->setSymbol(symbol);
    curve->setPen(QPen(Qt::NoPen));
    curve->attach(plot);
    curve->setAxes(QwtPlot::xBottom, QwtPlot::yRight);
    curve->setSamples(d1YLocalMax);

    // Y bands
    curve = new QwtPlotCurve(QObject::trUtf8("Перегибы"));
    curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve->setSymbol(symbol);
    curve->setPen(QPen(Qt::NoPen));
    curve->attach(plot);
    curve->setAxes(QwtPlot::xBottom, QwtPlot::yLeft);
    curve->setSamples(YBands);

    // D1Y global maximum
    QwtPlotMarker  *MaximumLabel = new QwtPlotMarker;
    symbol = new QwtSymbol;
    symbol->setStyle(QwtSymbol::Ellipse);
    symbol->setPen(QPen(Qt::blue));
    symbol->setBrush(QBrush(Qt::NoBrush));
    symbol->setSize(12);
    MaximumLabel->attach(plot);
    MaximumLabel->setAxes(QwtPlot::xBottom, QwtPlot::yRight);
    MaximumLabel->setLabel(QString::number(d1YGlobalMax[0].y()));
    MaximumLabel->setSymbol(symbol);
    MaximumLabel->setLabelAlignment(Qt::AlignHCenter | Qt::AlignBottom);
    MaximumLabel->setValue(d1YGlobalMax[0]);

    //target
    MaximumLabel = new QwtPlotMarker;
    MaximumLabel->attach(plot);
    MaximumLabel->setAxes(QwtPlot::xBottom, QwtPlot::yLeft);
    MaximumLabel->setLabel(QString::number(d1YGlobalMax[1].y()));
    MaximumLabel->setSymbol(symbol);
    MaximumLabel->setLabelAlignment(Qt::AlignHCenter | Qt::AlignTop);
    MaximumLabel->setValue(d1YGlobalMax[1]);

    PlotTextLabel *txt = new PlotTextLabel;
    txt->attach(plot);
    QwtText _text(QObject::trUtf8("aW = %1").arg(AW(d1YGlobalMax[1].y()), 0,
'e')); _text.setRenderFlags(Qt::AlignTop | Qt::AlignLeft); QFont fontLebelSens;
    fontLebelSens.setBold(true);
    fontLebelSens.setPixelSize(24);
    _text.setFont(fontLebelSens);
    txt->setText(_text);

    QwtLegend *legend = new QwtLegend;
    legend->setDefaultItemMode(QwtLegendData::Checkable);
    plot->insertLegend(legend);
    foreach(QwtPlotItem *item, plot->itemList())
    {
        QWidget *label = legend->legendWidget(plot->itemToInfo(item));
        if (label && label->inherits("QwtLegendLabel"))
            ((QwtLegendLabel*)label)->setChecked(true);
    }

    return plot;
}//*/

static QwtPlot *buildPlot(const QVector<double> &x, const QVector<double> &y) {
  QwtPlot *plot = new QwtPlot;
  plot->enableAxis(QwtPlot::yRight);
  plot->setCanvasBackground(QColor(Qt::white));
  plot->setStyleSheet("background-color:white;");

  QVector<QPointF> d[3];

  double *x_ = new double[x.size()];
  double *y_ = new double[y.size()];

  memcpy(x_, x.data(), x.size() * sizeof(double));
  memcpy(y_, y.data(), y.size() * sizeof(double));

  int l = x.size();

  spline_range *mSplineCoeffs; //= new spline_range[l - 1];

  SplineApprox(x_, y_, l, 50, &mSplineCoeffs);

  double step = __STEP(x.at(0), x.last());
  double xi = (double)x_[0];
  int t = 0;
  while (xi < x_[l - 1]) {
    calc_result res = calculateSplineAt(mSplineCoeffs, l - 1, xi);

    //! не рисовать точки выше +2
    if (res.Y <= CUTOFF_VALUES_ABOVE) {
      d[0].append(QPointF(xi, res.Y));
      d[1].append(QPointF(xi, res.d1Y));
      d[2].append(QPointF(xi, res.d2Y * 50));
    }
    xi += step;
  }
  QVector<double> d2yZeros = findD2YZeros(d[2]);
  QVector<QPointF> d1YLocalMax, YBands;

  double Global_d1y_Max = -INFINITY;

  foreach (double d, d2yZeros) {
    if ((d <= x.first()) || (d >= x.last()))
      continue;
    calc_result res_P = calculateSplineAt(mSplineCoeffs, l - 1, d);
    calc_result res_bP = calculateSplineAt(mSplineCoeffs, l - 1, d - step);
    calc_result res_aP = calculateSplineAt(mSplineCoeffs, l - 1, d + step);

    if ((res_P.d1Y > res_bP.d1Y) && (res_P.d1Y > res_aP.d1Y) &&
        res_P.d1Y <= 0.0 //! точки где производная  > 0 - ложное срабатывание
    ) {
      d1YLocalMax.append(QPointF(d, res_P.d1Y));
      if (res_P.d1Y > Global_d1y_Max)
        Global_d1y_Max = res_P.d1Y;
      YBands.append(QPointF(d, res_P.Y));
    }
  }

  // Удалить глобальный максимум из списка локальных
  QPointF d1YGlobalMax[2];
  t = 0;
  foreach (QPointF p, d1YLocalMax) {
    if (p.y() == Global_d1y_Max) {
      d1YGlobalMax[0] = p;
      d1YGlobalMax[1] = YBands.at(t);
      d1YLocalMax.remove(t);
      YBands.remove(t);
      break;
    }
    ++t;
  }

  delete[] x_;
  delete[] y_;
  delete mSplineCoeffs;

  QwtPlotCurve *curve;
  QPen pen;
  QwtSymbol *symbol;

  for (int i = 0; i < 2; ++i) {
    pen = QPen(QColor::fromHsvF((float)i / 3, 1.0, 1.0));

    symbol = new QwtSymbol;
    curve = new QwtPlotCurve(QString::number(i));
    symbol->setStyle(QwtSymbol::Cross);
    symbol->setPen(pen);
    symbol->setSize(5);
    curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve->setPen(pen);
    curve->setSymbol(symbol);
    curve->attach(plot);

    if (i == 0)
      curve->setAxes(QwtPlot::xBottom, QwtPlot::yLeft);
    else
      curve->setAxes(QwtPlot::xBottom, QwtPlot::yRight);

    curve->setSamples(d[i]);
  }
#if 0
  // Zero D2Y line
  pen = QPen(Qt::black);

  symbol = new QwtSymbol;
  curve = new QwtPlotCurve("Zero");
  symbol->setStyle(QwtSymbol::NoSymbol);
  symbol->setPen(pen);
  symbol->setSize(5);
  curve->setRenderHint(QwtPlotItem::RenderAntialiased);
  curve->setPen(pen);
  curve->setSymbol(symbol);
  curve->attach(plot);
  curve->setAxes(QwtPlot::xBottom, QwtPlot::yRight);
  double tx[] = {d[0].at(0).x(), d[0].at(d[0].size() - 1).x()};
  double ty[] = {0, 0};
  curve->setSamples(tx, ty, 2);
#endif
  // D1Y local maximums
  pen = QPen(Qt::red);

  symbol = new QwtSymbol;
  curve = new QwtPlotCurve(QObject::trUtf8("Локальные максимумы"));
  symbol->setStyle(QwtSymbol::Ellipse);
  symbol->setPen(pen);
  symbol->setBrush(QBrush(Qt::NoBrush));
  symbol->setSize(10);
  curve->setRenderHint(QwtPlotItem::RenderAntialiased);
  curve->setSymbol(symbol);
  curve->setPen(QPen(Qt::NoPen));
  curve->attach(plot);
  curve->setAxes(QwtPlot::xBottom, QwtPlot::yRight);
  curve->setSamples(d1YLocalMax);

  // Y bands
  curve = new QwtPlotCurve(QObject::trUtf8("Перегибы"));
  curve->setRenderHint(QwtPlotItem::RenderAntialiased);
  curve->setSymbol(symbol);
  curve->setPen(QPen(Qt::NoPen));
  curve->attach(plot);
  curve->setAxes(QwtPlot::xBottom, QwtPlot::yLeft);
  curve->setSamples(YBands);

  // D1Y global maximum
  QwtPlotMarker *MaximumLabel = new QwtPlotMarker;
  symbol = new QwtSymbol;
  symbol->setStyle(QwtSymbol::Ellipse);
  symbol->setPen(QPen(Qt::blue));
  symbol->setBrush(QBrush(Qt::NoBrush));
  symbol->setSize(12);
  MaximumLabel->attach(plot);
  MaximumLabel->setAxes(QwtPlot::xBottom, QwtPlot::yRight);
  MaximumLabel->setLabel(QString::number(d1YGlobalMax[0].y()));
  MaximumLabel->setSymbol(symbol);
  MaximumLabel->setLabelAlignment(Qt::AlignHCenter | Qt::AlignBottom);
  MaximumLabel->setValue(d1YGlobalMax[0]);

  // target
  MaximumLabel = new QwtPlotMarker;
  MaximumLabel->attach(plot);
  MaximumLabel->setAxes(QwtPlot::xBottom, QwtPlot::yLeft);
  MaximumLabel->setLabel(QString::number(d1YGlobalMax[1].y()));
  MaximumLabel->setSymbol(symbol);
  MaximumLabel->setLabelAlignment(Qt::AlignHCenter | Qt::AlignTop);
  MaximumLabel->setValue(d1YGlobalMax[1]);

  PlotTextLabel *txt = new PlotTextLabel;
  txt->attach(plot);
  QwtText _text(
      QObject::trUtf8("aW = %1").arg(AW(d1YGlobalMax[1].y()), 0, 'e'));
  _text.setRenderFlags(Qt::AlignTop | Qt::AlignLeft);
  QFont fontLebelSens;
  fontLebelSens.setBold(true);
  fontLebelSens.setPixelSize(24);
  _text.setFont(fontLebelSens);
  txt->setText(_text);

  QwtLegend *legend = new QwtLegend;
  legend->setDefaultItemMode(QwtLegendData::Checkable);
  plot->insertLegend(legend);
  foreach (QwtPlotItem *item, plot->itemList()) {
    QWidget *label = legend->legendWidget(plot->itemToInfo(item));
    if (label && label->inherits("QwtLegendLabel"))
      ((QwtLegendLabel *)label)->setChecked(true);
  }

  return plot;
}

void exit_DataErr() {
  QMessageBox::critical(
      NULL, QWidget::trUtf8("Ошибка данных"),
      QObject::trUtf8("Обнаружена ошибка в структуре данных!"),
      QMessageBox::Ok);
}

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  app.setApplicationName("SplineTest");

  QString fileName;

  if (argc < 2) {
    fileName = QFileDialog::getOpenFileName(
        NULL, QObject::trUtf8("Открыть"), QString(),
        QObject::trUtf8("CSV table (*.csv)"));
  } else {
    fileName = QString(argv[1]);
  }

  QFile datafile(fileName);
  if (!datafile.open(QIODevice::ReadOnly))
    return -1;

  QString data = datafile.readAll();
  datafile.close();

  QStringList lines = data.split("\r\n");

  QVector<double> x[2], y[2];

  enDataFormat dataFormat;
  switch (lines.at(0).split(";").size()) {
  case 2:
    dataFormat = OLD;
    break;
  case 9:
    dataFormat = R4_9;
    break;
  case 7:
    dataFormat = R4_7;
    break;
  }

  foreach (QString s, lines) {
    QStringList t = s.split(";");
    if (t.size() < 2)
      continue;

    // НОВЫЙ парсер
    switch (dataFormat) {
    case OLD:
      if (t.size() != 2) {
        exit_DataErr();
        return -2;
      }
      {
        double td = t.at(1).toDouble();
        x[0].append(td);
        x[1].append(td);
      }
      /** В старой таблице температура в милиградусах */
      y[0].append(t.at(0).toDouble() / 1000.0);
      break;
    case R4_9: // "Новый дравер"
      // 9 и 7 колонки - Y
      // первая;вторая - дата
      if (t.size() != 9) {
        exit_DataErr();
        return -2;
      }
      y[1].append(t.at(8).toDouble());
    case R4_7: // "cтарый драйвер"
      // 7 колонка - Y
      // первая;вторая - дата
      if ((t.size() != 9) && (t.size() != 7)) {
        exit_DataErr();
        return -2;
      }

      y[0].append(t.at(6).toDouble());
      {
        QString dateStr = t.at(0) + " " + t.at(1);
        QDateTime date =
            QDateTime::fromString(dateStr, "dd.MM.yyyy HH:mm:ss.zzz");
        double date_ms = date.toMSecsSinceEpoch();
        x[0].append(date_ms);
        x[1].append(date_ms);
      }
      break;
    }
  }

  // no data
  if ((x[0].size() != y[0].size()) || (x[0].size() == 0))
    return -2;

  /*
  alglib::real_1d_array al_x, al_y, t;
  al_x.setcontent(x.size(), x.data()); // аргумент
  al_y.setcontent(y.size(), y.data()); // значение

  alglib::spline1dinterpolant Sp[4];

  double v[x.size()];
  for(int i = 0; i < x.size(); ++i)
      v[i] = 1/100.0;
  t.setcontent(x.size(), v);

  alglib::spline1dbuildlinear(al_x, al_y, Sp[0]);
  alglib::spline1dbuildcubic(al_x, al_y, Sp[1]);
  alglib::spline1dbuildhermite(al_x, al_y, t, Sp[2]);
  alglib::spline1dbuildakima(al_x, al_y, Sp[3]);
  */

  /*
  QwtPlot *plot = buildPlot(x, y, Sp[2]);
  plot->setWindowTitle("->\t" + datafile.fileName());
  plot->showMaximized();
  //plot->show();

  halper h(plot, (QwtLegend*)plot->legend());
  */

  int plots_masq = 0;
  QwtPlot *plots[2];
  halper *halpers[2];

  for (int i = 0; i < 2; ++i)
    if (y[i].size() && !isnan(y[i].at(0))) {
      QwtPlot **plot = &plots[i];
      *plot = buildPlot(x[i], y[i]);
      halpers[i] = new halper(*plot, (QwtLegend *)(*plot)->legend());
      if (dataFormat != OLD)
        (*plot)->setAxisScaleDraw(
            QwtPlot::xBottom, new StockTimeScaleDraw(QObject::trUtf8("mm.ss")));
      plots_masq |= i + 1;
    }

  switch (plots_masq & 3) {
  case 0:
    return -3;
  case 1:
    plots[0]->showMaximized();
    QTimer::singleShot(100, halpers[0], SLOT(timerSlot()));
    break;
  case 2:
    plots[1]->showMaximized();
    QTimer::singleShot(100, halpers[1], SLOT(timerSlot()));
    break;
  case 3:
    for (int i = 0; i < 2; ++i) {
      plots[i]->resize(800, 600);
      plots[i]->show();
      plots[i]->move(70 + i * 150, 70 + i * 150);
      QTimer::singleShot(10 + i * 10, halpers[i], SLOT(timerSlot()));
    }
    break;
  }

  // QTimer::singleShot(100, &h, SLOT(timerSlot()));

  return app.exec();
}
